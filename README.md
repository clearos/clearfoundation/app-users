### ClearOS app-base

This application allows the administrator to manage all users, add, modify, 
delete assign one or more users to a group, create a group etc ...
This application offers the possibility to add extensions to add other functionality.



### License

* http://www.gnu.org/copyleft/gpl.html


### Install it

* This application is intalled by default in ClearOS.


### Contributing

* www.itotafrica.com
* avan.tech


### Developers and testers

* https://github.com/itotafrica


### Translators

* All the files concerning the translation are in the lang directory
